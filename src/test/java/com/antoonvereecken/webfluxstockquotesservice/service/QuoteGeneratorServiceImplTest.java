package com.antoonvereecken.webfluxstockquotesservice.service;

import com.antoonvereecken.webfluxstockquotesservice.model.Quote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

class QuoteGeneratorServiceImplTest {

    QuoteGeneratorServiceImpl quoteGeneratorService = new QuoteGeneratorServiceImpl();

    @BeforeEach
    void setUp() {
    }

    @Test
    void fetchQuoteStream() {

        Flux<Quote> quoteFlux = quoteGeneratorService.fetchQuoteStream(Duration.ofMillis(100L));

        quoteFlux.take(22000)
                .subscribe(System.out::println);

    }

    @Test
    void fetchQuoteStreamCountDown() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Flux<Quote> quoteFlux = quoteGeneratorService.fetchQuoteStream(Duration.ofMillis(100L));

        Consumer<Quote> quoteConsumerPrintLn = System.out::println;
        Consumer<Throwable> throwableConsumerErrorPrintLn = e -> System.out.println("Some error has occored");
        Runnable runWhenComplete = countDownLatch::countDown;

        quoteFlux.take(30) // .take(n) – number of items to emit from this Flux
                .subscribe(quoteConsumerPrintLn, throwableConsumerErrorPrintLn, runWhenComplete);

        countDownLatch.await();

        /**
         *      .subscribe(
         *          Consumer<? super T> consumer,
         *          Consumer<? super Throwable> errorConsumer,
         *          Runnable completeConsumer
         *      )
        */
    }
}