package com.antoonvereecken.webfluxstockquotesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxStockQuotesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxStockQuotesServiceApplication.class, args);
	}

}
