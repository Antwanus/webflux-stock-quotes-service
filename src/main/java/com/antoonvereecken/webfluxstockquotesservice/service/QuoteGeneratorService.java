package com.antoonvereecken.webfluxstockquotesservice.service;

import com.antoonvereecken.webfluxstockquotesservice.model.Quote;
import reactor.core.publisher.Flux;

import java.time.Duration;

public interface QuoteGeneratorService {

    Flux<Quote> fetchQuoteStream(Duration duration);
}
