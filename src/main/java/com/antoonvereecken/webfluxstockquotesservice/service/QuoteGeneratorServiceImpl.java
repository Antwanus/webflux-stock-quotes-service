package com.antoonvereecken.webfluxstockquotesservice.service;

import com.antoonvereecken.webfluxstockquotesservice.model.Quote;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.SynchronousSink;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;

@Service
public class QuoteGeneratorServiceImpl implements QuoteGeneratorService {

    private final Random random = new Random();
    private final List<Quote> prices = new ArrayList<>();

    public QuoteGeneratorServiceImpl() {
        this.prices.add(new Quote("AAPL", 160.16));
        this.prices.add(new Quote("MSFT", 77.74));
        this.prices.add(new Quote("GOOG", 847.24));
        this.prices.add(new Quote("ORCL", 49.51));
        this.prices.add(new Quote("IBM", 159.34));
        this.prices.add(new Quote("INTC", 39.29));
        this.prices.add(new Quote("RHT", 84.29));
        this.prices.add(new Quote("VMW", 92.21));
    }

    @Override
    public Flux<Quote> fetchQuoteStream(Duration period) {
        //todo get rid of the WTF

        // <T, S> Flux<T> generate(Callable<S> stateSupplier, BiFunction<S, SynchronousSink<T>, S> generator)
        // -> create quotes, iterating on each stock starting at index 0 (new topic)
        return Flux.generate(() -> 0, (BiFunction<Integer, SynchronousSink<Quote>, Integer>) (index, sink) -> {
                    Quote updatedQuote = updateQuote(this.prices.get(index));
                    sink.next(updatedQuote);
                    return ++index % this.prices.size();
                })
                // Zip this Flux with another Publisher source (=param), wait for both to emit one element and combine these elements once into a Tuple2.
                .zipWith(Flux.interval(period))
                .map(tuple2 -> tuple2.getT1())
                // Because values are generated in batches, we need to set their timestamp after their creation
                .map(quote -> {
                    quote.setInstant(Instant.now());
                    return quote;
                })
                .log("com.antoonvereecken.service.QuoteGenerator");
    }

    private Quote updateQuote(Quote q) {
        BigDecimal priceChange = q.getPrice().multiply(BigDecimal.valueOf(0.05 * this.random.nextDouble()));
        return new Quote(q.getTicker(), q.getPrice().add(priceChange));
    }
}
