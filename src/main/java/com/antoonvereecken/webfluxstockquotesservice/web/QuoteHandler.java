package com.antoonvereecken.webfluxstockquotesservice.web;

import com.antoonvereecken.webfluxstockquotesservice.model.Quote;
import com.antoonvereecken.webfluxstockquotesservice.service.QuoteGeneratorService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Component
public class QuoteHandler {

    private final QuoteGeneratorService quoteGeneratorService;

    public QuoteHandler(QuoteGeneratorService quoteGeneratorService) {
        this.quoteGeneratorService = quoteGeneratorService;
    }

    // response = Mono containing a flux.take()
    public Mono<ServerResponse> fetchQuotes(ServerRequest request) {
        // map request.param:'size' or default to 10
        int size = Integer.parseInt(request.queryParam("size").orElse("10"));

        return  ok().contentType(MediaType.APPLICATION_JSON)
                    .body(this.quoteGeneratorService.fetchQuoteStream(Duration.ofMillis(100))
                    .take(size),    // Take only the first N values from this Flux, if available.
                Quote.class);
    }

    public Mono<ServerResponse> fetchQuotesStream(ServerRequest request) {
        return  ok().contentType(MediaType.APPLICATION_NDJSON)
                    .body(this.quoteGeneratorService.fetchQuoteStream(Duration.ofMillis(100)),
                Quote.class);
    }

}
